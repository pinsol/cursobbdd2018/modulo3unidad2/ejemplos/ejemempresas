﻿DROP DATABASE IF EXISTS ejemEmpresas;
CREATE DATABASE IF NOT EXISTS ejemEmpresas;

USE ejemEmpresas;

CREATE TABLE IF NOT EXISTS persona (
  id int AUTO_INCREMENT,
  nombre VARCHAR(30),
  apellidos VARCHAR(30),
  poblacion varchar(30),
  fechaNac date,
  dato1 int,
  dato2 int,
  dato3 int,
  fechaTrabaja date,
  empresa int,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS empresa (
  codigo int AUTO_INCREMENT,
  nombre VARCHAR(30),
  poblacion VARCHAR(30),
  CP char(5),
  numTrabajadores int,
  PRIMARY KEY (codigo)
);

-- actualizamos la tabla con ALTER TABLE
  ALTER TABLE persona                                                -- tabla persona
   ADD CONSTRAINT FKPersonaEmpresa FOREIGN KEY(empresa)
   REFERENCES empresa(codigo) ON DELETE CASCADE ON UPDATE CASCADE;   -- actualizamos con la Clave Foranea